<?php

namespace GleSYS;

use Carpathia\Rest\Client as BaseClient;

class Client extends BaseClient {
    protected $_verify_ssl_validity = 0;
    private $_responseformat = 'json';
    protected $_baseurl = "https://api.glesys.com/";

    public function __construct($username = null , $apikey = null)
    {
        parent::__construct();
        $this->username = $username;
        $this->password = $apikey;
    }
    protected function setCurlOpts (&$curlHandle) {
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, $this->_verify_ssl_validity);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, $this->_verify_ssl_validity);
        parent::setCurlOpts($curlHandle);
    }
    protected function setAuth (&$curlHandle)    {
        if ($this->username !== null && $this->password !== null) {
            curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        }
    }


    public function request($action, array $data=array()) {
        $this->cp = true;
        $data["format"] = $this->_responseformat;
        $this->url = $this->_baseurl.$action;
        $this->setVerb('POST');
        $this->buildPostBody($data, true);
        $this->execute();
        $code = $this->responseInfo['http_code'];
        if ($this->responseInfo['content_type']=='application/json') {
            $json = json_decode($this->responseBody);
            if ($json) {
                $code = $json->response->status->code;
                $json = $json->response;
            }
            return array('code'=>$code, 'json'=>$json,'raw'=>$this->responseBody);
        }
        return array('code'=>$code,'raw'=>$this->responseBody);
    }
}
