<?php

namespace GleSYS\Api;

use GleSYS\Client as GleSYSClient;

class Domain {
	protected $client;
	public function __construct(GleSYSClient $client) {
		$this->client = $client;
	}

	/**
	 * Get a list of all servers on this account.
	 *
	 * Required arguments: None
	 * Optional arguments: None
	 */
	public function list() {
		$data = $this->client->request('server/list');
		if ($data['status']==200) {
			return $data['json']->servers;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get detailed information about a server
	 *
	 * Such as hostname, hardware configuration (cpu, memory and disk),
	 * ip adresses, cost, transfer, os and more. 'state' in the output will
	 * be set to null if the input parameter 'includestate' is set to false.
	 * If it is set to true it will return 'running', 'stopped' or 'locked'.
	 * The default value for 'includestate' is false, for performance reasons.
	 *
	 * Required arguments: serverid
	 * Optional arguments: includestate
	 */
	public function details($serverid, array $options = array()) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/details', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get status information about a server.
	 *
	 * This information may contain on/off-status, cpu/memory/hdd-usage,
	 * bandwidth consumption and uptime. Not all information is supported
	 * by all platforms.
	 *
	 * Required arguments: serverid
	 * Optional arguments: statustype
	 */
	public function status($serverid, array $options = array()) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/status', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Reboots a server.
	 *
	 * Does the same thing as server/stop with type set to reboot.
	 *
	 * Required arguments: serverid
	 * Optional arguments: None
	 */
	public function reboot($serverid) {
		$data = $this->client->request('server/reboot', ['serverid'=>$serverid]);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Shutdown, power off or reboot a server.
	 * Not all shutdown types are supported on all platforms.
	 * Only Xen supports a hard power off.
	 *
	 * Allowed type: soft, hard or reboot
	 *
	 * Required arguments: serverid
	 * Optional arguments: type
	 */
	public function stop($serverid, array $options = array()) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/stop', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Power up (boot) a server.
	 *
	 * Required arguments: serverid
	 * Optional arguments: None
	 */
	public function start($serverid) {
		$data = $this->client->request('server/start', ['serverid'=>$serverid]);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Create a new virtual server.
	 *
	 * An Xen server needs an ip-address when created. If you use the word
	 * 'any' as an argument, instead of an ip address, one will be choosen
	 * for you, automatically. If you dont supply the ip argument, one will
	 * be choosen for you.
	 *
	 * Required arguments: datacenter , platform , hostname , templatename ,
	 *		disksize , memorysize , cpucores , rootpassword , transfer
	 * Optional arguments: description , ip
	 */
	public function create($datacenter, $platform, $hostname, $templatename,
		$disksize, $memorysize, $cpucores, $rootpassword, $transfer,
		array $options = array()) {

		$options['datacenter'] = $datacenter;
		$options['platform'] = $platform;
		$options['hostname'] = $hostname;
		$options['templatename'] = $templatename;
		$options['disksize'] = $disksize;
		$options['memorysize'] = $memorysize;
		$options['cpucores'] = $cpucores;
		$options['rootpassword'] = $rootpassword;
		$options['transfer'] = $transfer;

		$data = $this->client->request('server/create', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Destroy a server
	 * Destroy a server and remove all files on it. This change is final and
	 * cannot be undone. You will NOT be asked for confirmation.
	 *
	 * Required arguments: serverid , keepip
	 *Optional arguments: None
	 */
	public function destroy($serverid, $keepip) {
		$data = $this->client->request('server/destroy', [
			'serverid' => $serverid,
			'keepip' => $keepip
		]);
		if ($data['status']==200) {
			return true;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Edit the configuration of a server.
	 *
	 * You can change such parameters as amount of cpu cores, memory, hdd and
	 * transfer. Most arguments are optional so you can change all, none or
	 * just a few of the parameters at a time.
	 *
	 * Required arguments: serverid
	 * Optional arguments: disksize , memorysize , cpucores , transfer , hostname , description
	 */
	public function edit($serverid, array $options = array()) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/edit', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Create an copy (clone) of another server.
	 *
	 * This copies all files on the server. It will not boot the clone when
	 * the cloning is done. This has to be done manually. The ip-adresses
	 * are not moved from the original servers. The clone will not have any
	 * ip-address so if one is needed it should be added by the
	 * ip/add-function. Cloning is only supported on the OpenVZ-platform.
	 *
	 * Required arguments: serverid , hostname
	 * Optional arguments: disksize , memorysize , cpucores , transfer , description , datacenter
	 */
	public function clone($serverid, $hostname, array $options = array()) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/clone', $options);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get the beancounter limits for a server.
	 *
	 * The limits lets you know if you have exceeded any of the limits that
	 * are set for your server. These are limitations such as allocated memory,
	 * open files and more. More information about the parameters returned by
	 * this function can be found at http://wiki.openvz.org/UBC . You can
	 * reset the failcount of a limit using the function server/resetlimit.
	 *
	 * Only for OpenVZ.
	 *
	 * Required arguments: serverid
	 * Optional arguments: None
	 */
	public function limits($serverid) {
		$options['serverid'] = $serverid;
		$data = $this->client->request('server/limits', $options);
		if ($data['status']==200) {
			return $data['json']->limits;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Resets the failcount.
	 * Resets the failcount for a beancounters limit to zero. See the
	 * documentation for server/limits for more information.
	 *
	 * Only for OpenVZ.
	 *
	 * Required arguments: serverid , type
	 * Optional arguments: None
	 */
	public function resetLimit($serverid, $type) {
		$data = $this->client->request('server/resetlimit', [
			'serverid' => $serverid,
			'type' => $type
		]);
		if ($data['status']==200) {
			return $data['json']->limits;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get VNC connections info.
	 *
	 * Get all the connection information you need to be able to connect to an
	 * server with VNC. This gives you console access and could be useful if
	 * you have locked yourself out using a firewall, or in a number of other
	 * situations.
	 *
	 * Required arguments: serverid
	 * Optional arguments: None
	 */
	public function console($serverid) {
		$data = $this->client->request('server/console', [
			'serverid' => $serverid
		]);
		if ($data['status']==200) {
			return $data['json']->console;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Reset the password.
	 *
	 * Reset the root-password of a OpenVZ-server to a password of your choice.
	 *
	 * Only for OpenVZ.
	 *
	 * Required arguments: serverid , rootpassword
	 * Optional arguments: None
	 */
	public function resetPassword($serverid, $password)	{
		$data = $this->client->request('server/resetpassword', [
			'serverid' => $serverid,
			'rootpassword' => $password
		]);
		if ($data['status']==200) {
			return $data['json']->server;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get a list of templates.
	 *
	 * Get a list of all operating system templates that are available when
	 * creating a server using the server/create-function.
	 *
	 * Required arguments: None
	 * Optional arguments: None
	 */
	public function templates() {

		$data = $this->client->request('server/templates');
		if ($data['status']==200) {
			return $data['json']->templates;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Lists the allowed arguments for some of the functions in this module
	 * such as disksize, cpucores etc.
	 *
	 * Required arguments: None
	 * Optional arguments: serverid
	 */
	public function allowedArguments(array $optios = array()) {
		$data = $this->client->request('server/allowedarguments',$options);
		if ($data['status']==200) {
			return $data['json']->argumentslist;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Return resource usage over time for server.
	 *
	 * Allowed resource: cpuusage, diskioread, diskiowrite, memoryusage,
	 * 		diskusage, beancounterfails, transfertotal, transferin, transferout
	 * Allowed resolution: minute, hour, day
	 *
	 * Required arguments: serverid , resource , resolution
	 * Optional arguments: None
	 */
	public function resourceUsage($serverid, $resource, $resolution) {
		$data = $this->client->request('server/resourceusage', [
			'serverid' => $serverid,
			'resource' => $resource,
			'resolution' => $resolution
		]);
		if ($data['status']==200) {
			return $data['json']->usage;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get cost for a server
	 *
	 * Required arguments: serverid
	 * Optional arguments: None
	*/
	public function costs($serverid) {
		$data = $this->client->request('server/costs', [
			'serverid' => $serverid
		]);
		if ($data['status']==200) {
			return $data['json']->costs;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);

	}
}
