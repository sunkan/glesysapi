<?php

namespace GleSYS\Api;

use GleSYS\Client as GleSYSClient;

class Domain {
	protected $client;
	public function __construct(GleSYSClient $client) {
		$this->client = $client;
	}

	/**
	 * Get a list of all domains on this account
	 *
	 * The glesysnameserver-item in the returned data shows if this domain
	 * actually uses glesys nameservers or not. It looks in the domains
	 * NS-records for .namesystem.se
	 *
     * Required arguments: None
	 * Optional arguments: None
	 */
	public function listDomains() {
		$data = $this->client->request('domain/list');
		if ($data['code']==200) {
			return $data['json']->domains;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}
	/**
	 * Add a domain to our dns-system.
	 *
	 * Some default records will be created on the domain. If the optional
	 * argument create_records is set to 0, only NS and SOA records will be
	 * created by default.
	 *
	 * Required arguments: domainname
	 * Optional arguments: primarynameserver , responsibleperson , ttl , refresh , retry , expire , minimum , createrecords
	 */
	public function add($domainname, array $options=array()) {
		$options['domainname'] = $domainname;
		$data = $this->client->request('domain/add', $options);

		if ($data['code']==200) {
			return $data['json']->domain;
		}
		throw new Exception($data['json']->status->text, $data['json']->status->code);
	}

	/**
	 * Get information on domainname
	 *
	 * Returns information like number of records, primarynameserver, ttl,
	 * expire, contactinfo and refresh
	 *
	 * Required arguments: domainname
	 * Optional arguments: None
	*/
	public function details($domainname) {
		$data = $this->client->request('domain/details',['domainname'=>$domainname]);

		if ($data['code']==200) {
			return $data['json']->domain;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * Edit soa for a domain in our dns-system
	 *
	 * Required arguments: domainname
	 * Optional arguments: primarynameserver , responsibleperson , ttl , refresh , retry , expire , minimum
	 */
	public function edit($domainname, array $options = array()) {
		$options['domainname'] = $domainname;
		$data = $this->client->request('domain/edit',$options);

		if ($data['code']==200) {
			return $data['json']->domain;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * Delete a domain from the dns system
	 *
	 * Required arguments: domainname
	 * Optional arguments: None
	 */
	public function delete($domainname) {
		$data = $this->client->request('domain/delete',['domainname'=>$domainname]);
		if ($data['code']==200) {
			return true;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * Update a records dns information
	 *
	 * Required arguments: recordid
	 * Optional arguments: ttl , host , type , data
	 */
	public function updateRecord($recordid, array $options = array()) {
		$options['recordid'] = $recordid;
		$data = $this->client->request('domain/updaterecord', $options);
		if ($data['code']==200) {
			return $data['json']->record;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * List records for a given domain (including recordid).
	 *
	 * Required arguments: domainname
	 * Optional arguments: None
	 */
	public function listRecords($domainname) {
		$data = $this->client->request('domain/listrecords',['domainname'=>$domainname]);
		if ($data['code']==200) {
			return $data['json']->records;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * Add a dns record to domainname
	 *
	 * Required arguments: domainname , host , type , data
	 * Optional arguments: ttl
	 */
	public function addRecord($domainname, $host ,  $type , $data, array $options = array()) {
		$options['domainname'] = $domainname;
		$options['host'] = $host;
		$options['type'] = $type;
		$options['data'] = $data;

		$data = $this->client->request('domain/addrecord', $options);
		if ($data['code']==200) {
			return $data['json']->record;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}

	/**
	 * Removes a dns record from a domain. Get the recordid using domain/listrecords.
	 *
	 * Required arguments: recordid
	 * Optional arguments: None
	 */
	public function deleteRecord($recordid) {
		$data = $this->client->request('domain/deleterecord',['recordid'=>$recordid]);
		if ($data['code']==200) {
			return true;
		}
		throw new Exception($data['json']->status->text,$data['json']->status->code);
	}
}